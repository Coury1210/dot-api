<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['web']], function () {
    // route user sessions
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
     return $request->user();

  });


Route::post('register', 'AuthController@register');


//
//Route::group(['middleware' => 'auth:api'], function() {
//
//	Route::get('articles', 'ArticleController@index');
//	Route::get('articles/{article}', 'ArticleController@show');
//	Route::post('articles', 'ArticleController@store');
//	Route::put('articles/{article}', 'ArticleController@update');
//	Route::delete('articles/{article}', 'ArticleController@delete');
//
//});

//routes below donot require token authentication
    Route::get('users', 'ArticleController@index');
    Route::patch('users', 'ArticleController@update');
    Route::delete('users', 'ArticleController@delete');

    Route::get('currentUser', 'ArticleController@show');

    Route::get('products', 'ProductController@index');
    Route::get('products/{id}', 'ProductController@show');
    Route::post('products', 'ProductController@store');
    Route::patch('products/{id}', 'ProductController@update');
    Route::delete('products/{id}', 'ProductController@destroy');

    Route::get('customers', 'CustomerController@index');
    Route::get('customers/{id}', 'CustomerController@show');
    Route::post('customers', 'CustomerController@store');
    Route::patch('customers/{id}', 'CustomerController@update');
    Route::delete('customers/{id}', 'CustomerController@destroy');


    Route::get('invoices','InvoiceController@index');
    Route::get('invoices/{id}', 'InvoiceController@show');
    Route::post('invoices', 'InvoiceController@store');
    Route::patch('invoices/{id}', 'InvoiceController@update');
    Route::delete('invoices/{id}', 'InvoiceController@destroy');

    Route::get('test', function(){
        return response()->json(['foo'=>'bar']);
    });



//routes require authentication with an api token viat the auth\ prefix of routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
   
});
//uses Controller i for authentication

//Route::post('logout', 'AuthController@logout');
//Route::post('login', 'AuthController@login');
//Route::post('recover', 'AuthController@recover');
//Route::post('register', 'AuthController@register');

//routes require user authentication with JWTAuth
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');

    
});