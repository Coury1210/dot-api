<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    //create a table in the database with the following fields
    Schema::create('invoices', function (Blueprint $table) {
        $table->increments('id');
        $table->string('customer');
        $table->string('products');
        $table->string('date');
        $table->timestamps();

    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {  //cancel table creation if a table of the same name exists

        Schema::dropIfExists('invoices');
    }
}
