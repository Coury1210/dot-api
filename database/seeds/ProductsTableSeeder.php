<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's clear the products table first
        Product::truncate();

        $faker = \Faker\Factory::create();
        
       

        // And now let's generate a few dozen products for our app:
        for ($i = 0; $i < 10; $i++) {
            Product::create([
                'name' => $faker->name,
                'price' => $faker->year,
                'description' => $faker->sentence,
                'service' => $faker->boolean:true,
            ]);
        }
    }
}

