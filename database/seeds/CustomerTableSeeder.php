<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { // Let's clear the customer table first
        Customer::truncate();

        $faker = \Faker\Factory::create();

       
        // And now let's generate a few dozen customers for our app:
        for ($i = 0; $i < 10; $i++) {
            Customer::create([
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                
            ]);
        }
    }
}
