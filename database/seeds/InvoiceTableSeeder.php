<?php

use Illuminate\Database\Seeder;
use App\Invoice;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  // Let's clear the invoice table first
        Invoice::truncate();

        $faker = \Faker\Factory::create();

     
        // And now let's generate a few dozen invoices for our app:
        for ($i = 0; $i < 10; $i++) {
            Invoice::create([
                'date' => $faker->date,
                'customer' => $faker->name,
                'products' => $faker->sentence,
            ]);
        }
    }
}
