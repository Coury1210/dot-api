-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2019 at 10:23 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestead`
--
CREATE DATABASE IF NOT EXISTS `homestead` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `homestead`;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstName`, `lastName`, `email`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Terrill', 'Stamm', 'shyann.kuhlman@lowe.biz', '+1 (245) 541-5462', '640 Krajcik Fords Suite 258\nVitachester, WI 40605-7217', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(2, 'Valerie', 'Mosciski', 'jaskolski.vesta@yahoo.com', '1-256-290-9985 x28868', '80580 Thompson Manors Apt. 510\nManntown, KY 12466-2788', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(3, 'Cyrus', 'Johnson', 'akemmer@yahoo.com', '347-267-2664 x179', '75507 Barrows Brook\nMyaville, LA 70290', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(4, 'Laisha', 'Crona', 'zmedhurst@hartmann.net', '559.699.2531 x84285', '972 Kennedy Rapid\nLeannonview, NJ 38972', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(6, 'Shanny', 'Little', 'izabella.muller@langworth.org', '(320) 776-6183', '3803 Esta Ville\nWest Earlenestad, GA 00243-3847', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(7, 'Ulises', 'Stokes', 'soconner@borer.net', '516-369-4015', '1199 Carolyn Parks Suite 940\nFrederiquetown, MI 31366-1152', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(8, 'Jarret', 'Wunsch', 'ybrakus@wilderman.net', '1-494-987-7617 x638', '94208 Bridgette Green\nDestinchester, AK 57764-4257', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(9, 'Lina', 'Murray', 'uokeefe@gmail.com', '(883) 272-0567', '97814 Tito Groves Apt. 602\nLake Edgar, OH 01379-6782', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(10, 'Danika', 'Mante', 'xprohaska@hotmail.com', '448-925-0481 x8782', '30084 Kohler Extensions Suite 160\nWillview, MN 60849', '2019-10-04 15:07:38', '2019-10-04 15:07:38'),
(11, 'Satos', 'Satos', 'hoi@dyt.nm', '0789247472847', '12street', NULL, NULL),
(12, 'Jasper Naty', 'Satos', 'hoi@dyt.nm', '0789247472847', '12street', NULL, NULL),
(13, 'Joana', 'Herds', 'herds@joana.vom', '07251498709', 'wandegeya', NULL, NULL),
(14, 'Judith', 'Love', 'loveme@tou.lo', '0002-9988776-87', 'DC,WASHINGTON', '2019-10-05 13:39:41', '2019-10-05 13:39:41'),
(15, 'Abu', 'Dongo', 'doto@tyr.git', '0002-9988776-87', 'DC,WASHINGTON', '2019-10-05 13:41:30', '2019-10-05 13:41:30'),
(16, 'Abu', 'Dongo', 'doto@tyr.git', '0002-9988776-87', 'DC,WASHINGTON', '2019-10-05 13:41:38', '2019-10-05 13:41:38'),
(17, 'Abdul', 'Karim', 'karat@mus.li', '900045-10876', 'congo', '2019-10-05 13:58:02', '2019-10-05 13:58:02'),
(18, 'Abdulloi', 'SasaKarim', 'akikarat@mus.li', '900045-10876-0987', 'brazavillecongo', '2019-10-05 14:01:59', '2019-10-05 14:01:59'),
(19, 'Danstan', 'Seka', 'sheikakikarat@mus.li', '900045-10876-0987', 'brazavillecongo', '2019-10-08 04:54:56', '2019-10-08 04:54:56'),
(20, 'Man', 'Sam', 'samakikarat@mus.li', '900045-10876-0987', 'tanzania', '2019-10-08 08:05:47', '2019-10-08 08:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `customer`, `products`, `date`, `updated_at`, `created_at`) VALUES
(1, 'Esteban Kuhlman PhD', 'Fugit molestiae rem voluptatem architecto quos eum quia.', '1982-06-10', '2019-10-04 18:07:41', '2019-10-04 18:07:41'),
(2, 'Miss Tamara Gorczany II', 'Labore rem veniam reiciendis natus qui sit excepturi.', '1970-09-22', '2019-10-04 18:07:41', '2019-10-04 18:07:41'),
(6, 'Tomasa Johns', 'Et possimus qui veniam pariatur.', '2000-10-08', '2019-10-04 18:07:41', '2019-10-04 18:07:41'),
(7, 'Prof. Rita Kunze', 'Quo unde quos recusandae et consequatur.', '1982-10-22', '2019-10-04 18:07:41', '2019-10-04 18:07:41'),
(8, 'Miss Naomi Turner I', 'Sit id ut quidem.', '1979-08-27', '2019-10-04 18:07:41', '2019-10-04 18:07:41'),
(9, 'Devante Rosenbaum', 'Suscipit asperiores hic quas placeat.', '1979-11-30', '2019-10-04 18:07:41', '2019-10-04 18:07:41'),
(11, 'Kagimu', 'rice,gas,g/nuts', '04/10/2019', '2019-10-05 22:12:31', '2019-10-05 22:12:31'),
(12, 'Kagimu Bosco', 'rice,gas,g/nuts', '04/10/2019', '2019-10-06 13:10:13', '2019-10-06 13:10:13'),
(13, 'Kagimu Bosco', 'rice,gas,g/nuts', '04/10/2019', '2019-10-06 17:06:39', '2019-10-06 17:06:39'),
(14, 'Kagimu Bosco', 'rice,gas,g/nuts', '04/10/2019', '2019-10-06 17:09:06', '2019-10-06 17:09:06'),
(15, 'Kagimu Bosco', 'rice,gas,g/nuts', '04/10/2019', '2019-10-07 11:34:58', '2019-10-07 11:34:58'),
(16, 'Kagimu Bosco', 'rice,gas,g/nuts', '04/10/2019', '2019-10-07 12:06:48', '2019-10-07 12:06:48'),
(17, 'Kagimu Bosco', 'rice,gas,g/nuts', '04/10/2019', '2019-10-08 07:53:00', '2019-10-08 07:53:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_10_03_131819_create_product_table', 2),
(9, '2019_10_03_132252_create_products_table', 2),
(10, '2019_10_03_132335_create_customers_table', 2),
(11, '2019_10_03_132414_create_invoice_table', 2),
(12, '2019_10_03_214711_adds_api_token_to_users_table', 2),
(13, '2019_10_04_154824_user_verification_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'IufyD9lX4HHCx1XahNAAxsIL7Q4YdxzXuCksbrcA', 'http://localhost', 1, 0, 0, '2019-10-03 23:02:37', '2019-10-03 23:02:37'),
(2, NULL, 'Laravel Password Grant Client', 'jTz9N7C8Tp3LdkbWYwWkU0Ezs28uR6gM1zAFcgiF', 'http://localhost', 0, 1, 0, '2019-10-03 23:02:38', '2019-10-03 23:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-10-03 23:02:38', '2019-10-03 23:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `service`, `created_at`, `updated_at`) VALUES
(1, 'Marvin Hane', 1977, 'Sed voluptatem sint voluptate repellendus quo quos.', '0', '2019-10-04 15:07:39', '2019-10-04 15:07:39'),
(2, 'Prof. Jennifer Barton', 1992, 'Quisquam tempore dignissimos dolores qui.', '1', '2019-10-04 15:07:39', '2019-10-04 15:07:39'),
(3, 'Mrs. Katharina O\'Reilly MD', 1999, 'Sed quia itaque quas asperiores aut fuga officia.', '1', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(4, 'Jamey Breitenberg Sr.', 1986, 'Aperiam expedita quis nisi voluptas quas accusantium.', '1', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(6, 'Ervin Mann', 1975, 'Dignissimos totam in ad expedita.', '0', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(7, 'Miss Rahsaan Johnston', 1972, 'Vel eius nihil nisi libero quisquam qui.', '1', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(8, 'Dr. Jamison Koss I', 1972, 'Qui cumque quisquam velit autem enim.', '1', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(9, 'Ena Kunde V', 1973, 'Laboriosam dolorum deserunt consequatur id.', '0', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(10, 'Prof. Otilia Ratke', 1984, 'Voluptas eaque nostrum quidem est adipisci et.', '0', '2019-10-04 15:07:40', '2019-10-04 15:07:40'),
(11, 'Laptop', 2000, 'me i am so good for this laptop, 3i core hp', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_verified`, `remember_token`, `user_id`, `token`, `created_at`, `updated_at`, `api_token`) VALUES
(1, 'Administrator', 'admin@test.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 1, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(2, 'Giovanna Ankunding', 'leffler.jayden@gmail.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 0, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(3, 'Freeman Purdy', 'layne78@hand.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 0, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(4, 'Elsie Langworth', 'yswaniawski@hotmail.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 0, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(5, 'Shaina Reichel', 'bayer.esperanza@hotmail.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 1, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(6, 'Mohammed Grimes', 'wilkinson.dora@hotmail.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 1, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(7, 'Shana Bradtke', 'pacocha.clinton@rogahn.info', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 1, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(8, 'Mrs. Malika Hilpert', 'yvolkman@yahoo.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 1, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(9, 'Mallory Walsh Jr.', 'fred48@hotmail.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 1, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(10, 'Mr. Delaney Stamm Jr.', 'botsford.gina@rau.info', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 0, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(11, 'Eldora Crist', 'eichmann.deanna@koepp.com', '$2y$10$WkqEdpl3AwGsdzUbrcPm5.I5aldxK9mYbOQ6mCLh2GfjSd6lLrd02', 0, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 15:07:43', '2019-10-04 15:07:43', NULL),
(12, 'Santos', 'kalo@uio.bo', 'daf316f5bc1d1206bfe78ad9932d4f35', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 16:42:20', '2019-10-04 16:42:20', NULL),
(13, 'Jack Bauer', 'kalio@uio.bo', 'daf316f5bc1d1206bfe78ad9932d4f35', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 16:54:01', '2019-10-04 16:54:01', NULL),
(14, 'jane boo', 'kal@eoo.bot', '6c1fcdf13d90ae7061c09cbe4937cefe', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 17:04:56', '2019-10-04 17:04:56', NULL),
(15, 'mama joe', 'mal@eo.bot', 'e6e0b6c15a029e0c92eddc4411882cae', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 17:47:14', '2019-10-04 17:47:14', NULL),
(16, 'mamato joe', 'mallow@eo.bot', 'e6e0b6c15a029e0c92eddc4411882cae', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 17:52:54', '2019-10-04 17:52:54', NULL),
(17, 'mamatoso joe', 'yimallow@eo.bot', 'e6e0b6c15a029e0c92eddc4411882cae', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 17:54:30', '2019-10-04 17:54:30', NULL),
(18, 'maoso joe', 'ow@eo.bot', '$2y$10$qgY/u5P.C36VlNAqyZMjUepyd/wzEoQ9hdDnDTtRGlhflRz6P0vVe', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 18:12:30', '2019-10-04 18:12:30', NULL),
(19, 'joe non', 'ow@eo.koi', '$2y$10$K2wl0uqcToBTFH29sZsk5OwjLkgOGmeOAT/zHRzL5eUffdnZ/6/wq', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 18:19:19', '2019-10-04 18:19:19', NULL),
(20, 'no jokes', 'tow@beeo.koi.or', '$2y$10$83eE/nVZ27xsGXrPG1AIvOokrhc7ucVffBibbaC73.Mf8KrbmESfS', NULL, NULL, '26', '058yLnFwT2ytUdxnEgLDrvTSj1nVKH', '2019-10-04 18:25:08', '2019-10-04 18:25:08', NULL),
(27, 'Kagimu Bosco', 'coury0@yahoo.com', '$2y$10$HzoEnKYzBFbmaWTTDMu5Eui6PBORJKF4SZnfRJD59z5gBhEbxd6w2', NULL, NULL, NULL, NULL, '2019-10-04 20:42:09', '2019-10-04 20:42:09', NULL),
(29, 'Me and You', 'yata@yahoo.com', '$2y$10$GDa0Mb219xJ8BynGf539TeFsPcS5FMFm0HigRVT7TJcvq4hzgW9jG', NULL, NULL, NULL, NULL, '2019-10-04 20:54:24', '2019-10-04 20:54:24', NULL),
(30, 'Me and You', 'yatapo@yahoo.com', '$2y$10$XmKWuYC1klNRwnCKaPUaJeqkVo0cBcasw7IfWIWRvYY/8Hrwc1nFi', NULL, NULL, NULL, NULL, '2019-10-04 20:54:52', '2019-10-04 20:54:52', NULL),
(31, 'MetoYou', 'yataporo@yahoo.com', '$2y$10$5APVJXOdMGh9WUqfiEfROO9epyffD.ii76ElKrfTmNdsugLBQPJBu', 1, NULL, NULL, NULL, '2019-10-04 22:04:12', '2019-10-04 22:04:12', NULL),
(32, 'Kasumba Samuel', 'guat@poil.duo', '$2y$10$241MkkL6CPm.8sKlkF2FC.cka71oGu4hcXJ1vD5SafxI6oGv73F.2', NULL, NULL, NULL, NULL, '2019-10-06 06:59:47', '2019-10-06 06:59:47', NULL),
(33, 'Mata Polo', 'guatpolo@poil.duo', '$2y$10$SXyJcrK7sElFYWuw/xXivO0XzkJQtHjwyvLrXS8HAU/tXJYIy38pi', NULL, NULL, NULL, NULL, '2019-10-06 07:32:40', '2019-10-06 07:32:40', NULL),
(34, 'Agip Total', 'giloko@flt.oi', '$2y$10$8NQLz6CfGe3SJvycPyyIOuwh73fQlEDo83v7p1/ippfJTF1COzn3q', NULL, NULL, NULL, NULL, '2019-10-07 13:15:56', '2019-10-07 13:15:56', NULL),
(35, 'Kiconco Catherine', 'cathy@gmail.com', '$2y$10$.nCD6W2.TnFuAmsZhkeDD.J9yZ8CPwChb3xkMQGqkMgjisOmDcP8K', NULL, NULL, NULL, NULL, '2019-10-07 14:06:56', '2019-10-07 14:06:56', NULL),
(36, 'Josam Port', 'cjosh@gmail.com', '$2y$10$kGW78GVKUysQnw3Mix7Umuu1jBtKUfvFzR/hj2N3.w.KNU3zOXtXi', NULL, NULL, NULL, NULL, '2019-10-07 14:28:06', '2019-10-07 14:28:06', NULL),
(37, 'Rwot Joshua', 'joshrwo@gmail.com', '$2y$10$TIzuep1qsTob7hZ.HF/BaeGZ8nFVQksiKORvpM0ft3hr9wJRRgXri', NULL, NULL, NULL, NULL, '2019-10-07 14:43:22', '2019-10-07 14:43:22', NULL),
(38, 'Bazinduse Fred', 'fredBaz@gmail.com', '$2y$10$RTNgPJ6qzIrqVBvJRiwrV.6XcNILrrETV/MgYE80AstTlb04pA6KK', NULL, NULL, NULL, NULL, '2019-10-07 14:47:46', '2019-10-07 14:47:46', NULL),
(39, 'Esther Mani', 'fmanies@gml.com', '$2y$10$NVHPz3mZEnU4ciu8FL9TpOBvsc9TRblk.Wf09aeFud2YChidgblN.', NULL, NULL, NULL, NULL, '2019-10-07 14:52:59', '2019-10-07 14:52:59', NULL),
(40, 'Devie Grace', 'godrules@gmoil.com', '$2y$10$nvdIMasdCcB0GwKV/UUJyO6WvIF/EsRKXds4AKSMEo3wN2b38/Ega', NULL, NULL, NULL, NULL, '2019-10-07 14:59:21', '2019-10-07 14:59:21', NULL),
(41, 'Duta Sam', 'kas@dfdg.coi', '$2y$10$wKchpwr8QvBvxP8LByuWQepO60X1FEkAEYw/nuY80n/LNVRkBgjvW', NULL, NULL, NULL, NULL, '2019-10-07 18:36:27', '2019-10-07 18:36:27', NULL),
(42, 'So good', 'cofi@rafd.git', '$2y$10$/cPY6LnZwcnekdrOnu2dY.eF0EXakrVNXtSiOirFZ/ewewLaufUDK', NULL, NULL, NULL, NULL, '2019-10-07 18:47:30', '2019-10-07 18:47:30', NULL),
(43, 'Butt Git', 'cofi@rafd.gi', '$2y$10$vlhcemebDX.OK7nfeUvl8eb1o2ExXCwsrHPrGGtLjKjBO735eamwu', NULL, NULL, NULL, NULL, '2019-10-07 18:52:50', '2019-10-07 18:52:50', NULL),
(44, 'Butts Git', 'coyyfi@rafd.gi', '$2y$10$/wr2JjLq8pUOLi06d.Bxhuy4uGVE3j.3Uj1ohI5jQDC4T18pkqBhO', NULL, NULL, NULL, NULL, '2019-10-07 18:56:03', '2019-10-07 18:56:03', NULL),
(45, 'Butts Gitopi', 'coyyfi@rafd.gil', '$2y$10$.elj5lbPHadsJRMfv2S6luim64NwnQFS0pw3M/bNxVn46efIShW0i', NULL, NULL, NULL, NULL, '2019-10-07 18:58:41', '2019-10-07 18:58:41', NULL),
(46, 'Buttses Gitopi', 'bicoyyfi@rafd.gil', '$2y$10$Djy8Pn/LucgfmY8RnwQBvO/a2k6XYbKFLDn1hV/25pwG149ubgI7a', NULL, NULL, NULL, NULL, '2019-10-07 19:00:17', '2019-10-07 19:00:17', NULL),
(47, 'AButtses Gitopi', 'pbicoyyfi@rafd.gil', '$2y$10$fHa/UGbcjEMhYCaGVID7Z.w.jcgs.4wompj5XUxzsSv8KRDfubjei', NULL, NULL, NULL, NULL, '2019-10-07 19:02:37', '2019-10-07 19:02:37', NULL),
(48, 'AButtses Gitopip', 'pbicoyyfi@rafd.gill', '$2y$10$ftAmpvL.vUTBZd9r5VwZ6OoiN9Y93fWOqiguReEixEtxvrDxq3vfu', NULL, NULL, NULL, NULL, '2019-10-07 19:03:21', '2019-10-07 19:03:21', NULL),
(49, 'AButtses Gitopipp', 'pbicoyyfi@rafd.gillt', '$2y$10$8ddkCmCybwzCb4EKMGNLG.YeEjB8HYEPMJnAB20eNcYwA4B6Ydjma', NULL, NULL, NULL, NULL, '2019-10-07 19:04:57', '2019-10-07 19:04:57', NULL),
(50, 'AButtsesim Gitopipp', '12pbicoyyfi@rafd.gillt', '$2y$10$ylOoiFApteoLQ4yPidZAd.Q6qnlwviaDSZGmPzk9q5DBmwPcK3Yq.', 1, NULL, NULL, NULL, '2019-10-07 19:09:50', '2019-10-07 19:09:50', NULL),
(51, 'Danstan', 'adupo@rafd.gillt', '$2y$10$J9pWIk6Ora.WnTXPnQo7w.tEtTxF/sLa5sLBDLZgW6XZaznMygRlO', NULL, NULL, NULL, NULL, '2019-10-08 01:55:51', '2019-10-08 01:55:51', NULL),
(52, 'Karl Danstan', 'dsadupo@rafd.gillt', '$2y$10$hzADeFkZAiRAjZ0PdKrlaOV4AFqd0IuI2zCDX8GyuLMyX2ldbh5cm', NULL, NULL, NULL, NULL, '2019-10-08 02:02:53', '2019-10-08 02:02:53', NULL),
(53, 'Karl Danstanit', 'dsadupot@rafd.gillt', '$2y$10$smTS8oILg4ua7nJexx2P9eBs6PRW8kN785R5DRPZWdHDyfct4f4OC', NULL, NULL, NULL, NULL, '2019-10-08 02:10:43', '2019-10-08 02:10:43', NULL),
(54, 'Sara Hayes', 'hayes@rafd.online', '$2y$10$2Lp/prMQTJNHjFlWYDq6u.DuRmvmN8ehY8EpgNhgNwvRb9iih1sWi', NULL, NULL, NULL, NULL, '2019-10-08 02:13:02', '2019-10-08 02:13:02', NULL),
(55, 'Sarani Hayes', 's.hayes@rafd.online', '$2y$10$9iD.JQMWusb.EsljSopRSuPNSL1CWwRMEvlTmy.BgaJSrfJEVpbE.', NULL, NULL, NULL, NULL, '2019-10-08 02:23:18', '2019-10-08 02:23:18', NULL),
(56, 'TSarani Hayes', 'st.hayes@rafd.online', '$2y$10$CIoJJECJAr6UZ2IAC2I1COJpTW.6HhqdKQu5/ZAupEN43UvJg/vQ.', NULL, NULL, NULL, NULL, '2019-10-08 02:26:28', '2019-10-08 02:26:28', NULL),
(57, 'Nakitto Evelyn', 'stluke@rafd.online', '$2y$10$IW5XjtoU9lRS0DTZpG6l2.IRBgxXCxW2WYoJzOGHhLHZOgrPMWCC.', NULL, NULL, NULL, NULL, '2019-10-08 03:51:58', '2019-10-08 03:51:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_verifications`
--

CREATE TABLE `user_verifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_verifications`
--

INSERT INTO `user_verifications` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, '22', '6w7yuGYIkctWkzxnEMIVzqd09x8gXz', NULL, NULL),
(2, '29', 'yTvbT24VWLReDUKBW8f4Se1lMve5vF', NULL, NULL),
(3, '30', 'bF5QrQekw687F9JN4RT4AUaDrlxuj0', NULL, NULL),
(4, '31', 'elWOnc8rzv829qYMtrZcEpXDqlXhs8', NULL, NULL),
(5, '32', 'wbnfKkQFu63En3REMfRwHMAS08UG8e', NULL, NULL),
(6, '33', 'C7pDqos82BkrHgGrQyaouZipHcrWn9', NULL, NULL),
(7, '34', 'SNmV7zACvky8mKD50ZIRpSaoxoITgJ', NULL, NULL),
(8, '35', '5aZJycoMkgnBzHXFfMMLuLg5t7zgHK', NULL, NULL),
(9, '36', 'x1117DbPaABboaj2Co8tWYkCpau57N', NULL, NULL),
(10, '37', 'QFaj4tmJy6hgfYFbjLx0JYXZl0NmqP', NULL, NULL),
(11, '38', 'xgn2slx8hejKAZf8NBdHVUpAn5bhJv', NULL, NULL),
(12, '39', 'PqDLCLkAc3SOHy69wcpEIQBcmkSov7', NULL, NULL),
(13, '40', 'wXwagRBLryqD1ZyNDI7s73lF3k4P2N', NULL, NULL),
(14, '41', 'bnsHUR0JSn411Dg0NHOoHIboh93dFe', NULL, NULL),
(15, '42', 'kvvUD70tCgVGUZJRyHCSUjZOTFX2Q1', NULL, NULL),
(16, '43', 'j3eiUWHm0jLDTphpzlwTfXQm9QuNQc', NULL, NULL),
(17, '44', 'G0hE4xbLkFXISgpCbfPq6cUsma5LSx', NULL, NULL),
(18, '45', '4LpUnOFzc6QpmwLHVGkMCIMGG26cE1', NULL, NULL),
(19, '46', 'JaLVm2aumuK6vInhykZrU1hO8Z8IAt', NULL, NULL),
(20, '47', 'PXnH86D8s0Z3feMOAJfpwiDLN0nmkK', NULL, NULL),
(21, '48', 'BJBY8budwfgCUHfS84295KuZV19BN2', NULL, NULL),
(22, '49', 'UyF4E7g5vnuTOhWJ1z5QHItjTNCilG', NULL, NULL),
(23, '50', '4PmcBCHAc5rtQnqbn8Q8A1fZwkOeRt', NULL, NULL),
(24, '51', 'Ij2j1UW8pd571PDRkj3x4LG4xwN7dh', NULL, NULL),
(25, '52', 'Md3P8EKOj8h7O3DbMWbH1efS9o2mpI', NULL, NULL),
(26, '53', 'g7Hazo11i6rfqdsSMPWb5uDNPNpC0H', NULL, NULL),
(27, '54', 'wBfertNWGNbdFn3p8E7unP7rCrUQNd', NULL, NULL),
(28, '55', 'XuiY57YuI5T41f1wl9VkXSqyYXHXit', NULL, NULL),
(29, '56', 'odR3UijsQkDThOMd0ggOQCUYisCt36', NULL, NULL),
(30, '57', 'IYbjPG47E73LOY3fNQPVm420g7HkhD', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `user_verifications`
--
ALTER TABLE `user_verifications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `user_verifications`
--
ALTER TABLE `user_verifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Database: `laravel5restapi`
--
CREATE DATABASE IF NOT EXISTS `laravel5restapi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `laravel5restapi`;
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(10) UNSIGNED NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"homestead\",\"table\":\"products\"},{\"db\":\"homestead\",\"table\":\"users\"},{\"db\":\"homestead\",\"table\":\"user_verifications\"},{\"db\":\"homestead\",\"table\":\"customers\"},{\"db\":\"homestead\",\"table\":\"invoices\"},{\"db\":\"homestead\",\"table\":\"oauth_clients\"},{\"db\":\"homestead\",\"table\":\"oauth_refresh_tokens\"},{\"db\":\"homestead\",\"table\":\"oauth_auth_codes\"},{\"db\":\"homestead\",\"table\":\"oauth_access_tokens\"},{\"db\":\"homestead\",\"table\":\"migrations\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float UNSIGNED NOT NULL DEFAULT 0,
  `y` float UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2019-10-08 06:14:28', '{\"Console\\/Mode\":\"collapse\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
