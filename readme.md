---------------------------------------------------------------------
--------This is an API developed in Laravel Framework--------------
----------------------------------------------------------------------
//App requires lates Laravel framework of 5.6 and PHP V7.3.1 
//with Apache and MYSQL functionality
//Has JSON returnsdata in arrays and a pagination of 5
//Based on PHP
//JavaScript enabled
//returns exceptions on errirs and unsatisfied fields from enhanced by Http Provisers
//more embedded codes inside that can enhace security and performance for user verifications by email and validation of data returns

App was successfully Tested in postman for result consistency

Users are registered so that they are able to create new products, customers and invoices when 
they are logged in

//Use authentication is impemented using JWTAuthServiceProvider and not Passport for simplicity and proper validation
Users can also perform the above functions when they are not logged in

App uses Laravel migration models to create tables in a database on the server and seedlings to create fake data in the database for testing purposes

Users are stored in a mysql database when server requests for registration are accepted
=============================================================================
find the sql file for the database in the app directory
========================================================
use the following url requests to access the end points

GET : http://localhost:8000/api/products        //views all created products from the database
GET : http://localhost:8000/api/customers      //views all created customers from the database
GET : http://localhost:8000/api/invoices       //views all created invoices from the database
GET : http://localhost:8000/api/products        //views all users with their details from the database
GET : http://localhost:8000/api/invoices/{id}   //return an invoice of the given id value from the database if it exists
===========================================================================================================================================
POST: http://localhost:8000/api/customers             //register new customers
POST: http://localhost:8000/api/invoices             //registers new invoice details
POST: http://localhost:8000/api/products             //register new products details
POST: http://localhost:8000/api/register               //register new admin users
=================================================================================================================================================================
DELETE:http://localhost:8000/api/customers/{id}         //searches customer of the given id and deletes them
DELETE:http://localhost:8000/api/invoices/{id}          //searches invoice of the given id and deletes them
DELETE:http://localhost:8000/api/products/{id}        //searches product of the given id and deletes them
================================================================================================================================================================================================================
PATCH: http://localhost:8000/api/invoices/{id}         //searches for invoice of the given id from the database if it exists and updates it with the given information from the server
PATCH: http://localhost:8000/api/products/{id}         //searches a for product of the given id from the database if it exists and updates it with the given information from the server
PATCH: http://localhost:8000/api/customer/{id}         //searches for a customer of the given id from the database if it exists and updates it with the given information from the server

