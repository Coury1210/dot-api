<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
 //fields to be created in the model for seeding
 protected $fillable = ['date', 'customer','products','updated_at','created_at'];
}
