<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //fields to be created in the model for seeding
    protected $fillable = ['name', 'price' , 'description' , 'service'];
}
