<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ArticleController extends Controller
{
    public function index()
    {
        return User::paginate(5);
    }

    public function show(User $user)
    {
        return $user;
    }
    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return response()->json($user, 200);
    }

    public function delete(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }

}
