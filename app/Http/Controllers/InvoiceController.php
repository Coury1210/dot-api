<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, DB, Hash, Mail;

use App\Invoice;

class InvoiceController extends Controller
{
    public function index()
    {
        return Invoice::paginate(5);
    }
    public function store(Request $request)
    { 
        $credentials = $request->only('date','customer','products');
//create rules for validation
      $rules = [
          'date'=> 'required',
          'customer' => 'required|max:255',
          'products'=> 'required',];
    //create validation varianles by requesting posted server parameter values       
     $date =$request->date;
    $customer = $request->customer;
      $products = $request->products;

      //create a validator for given rules
      $validator = Validator::make($credentials, $rules);
      //check for duplicate data
      $check = DB::table('invoices')->where('customer',$customer)->first();

//create if and not statements to kill processes and execute errors
      if($validator->fails()) {
          return response()->json(['success'=> false, 'error'=> $validator->messages()]);
      }

      elseif(is_null($customer)){
         
              return response()->json([
                                          'success'=> false,
                                          'message'=> 'Enter customer name, field is empty',
                                      ]);
          }
          elseif(is_null($products)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter products price field is empty',
                                    ]);
        }
     //if every thin goes well and the validation is successful, insert records in the database
          DB::table('invoices')->insert(['date'=>$date,'customer'=>$customer,'products'=>$products,]);
       
        return response()->json(['success'=> true, 'message'=> 'Invoice details have been stored successfully']);

        }
//use the request http method to collect user data from the serve as they post it
    public function show(Request $request)
    {    $buyer = $request->id;
        $viewRecords = DB::table('invoices')->where('id',$buyer)->first();
        if(is_null($viewRecords)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'There is no invoice ' .$buyer. ' please try again',
                                    ]);
        }
//return an array of data where the given id exists
        $result= DB::table('invoices')
                       ->where('id', $buyer)
                          ->get();
             return $result->toArray();
    }
//call to be executed in update resource router
    public function update(Request $request)
    {//user input variables from the server
        $buyer=$request->id;
        $update =$request->product;
        $viewRecords = DB::table('invoices')->where('id',$buyer)->first();
        
      //return errors for empty fields 
        if(is_null($buyer)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter and invoice id please',
                                    ]);
        }
        elseif(is_null($update)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter product field is empty',
                                    ]);
        }

        elseif(is_null($viewRecords)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'There is no invoice ' .$buyer. ' please provide invoice id to update',
                                    ]);
        }
        DB::table('invoices')->where('customer', $buyer)->update(['products' =>$update ]);
        return response()->json(['success'=> true, 'message'=> 'Invoice details have been updated successfully']);

    }
//delete data from the table where an id is provided
    public function destroy(Request $request)
    { 
        //create variables to store the data from the user
        $owner = $request->id;
        $record = DB::table('invoices')->where('id',$owner)->first();

//start validation of field to make sure thee user submits all required fields
        if(is_null($owner)){
            return response()->json(['success'=> false, 'message'=> 'Please provide the invoice id']);
        }
        elseif(is_null($record)){
            return response()->json(['success'=> false, 'message'=> 'Invoice doesnot exist or was already deleted']);
        }
        $data = DB::table('invoices')->where('id',$owner)->delete();

       if(!is_null($data)){
         
            return response()->json([
                                        'success'=> true,
                                        'message'=> 'The invoice has been deleted ']);
        }
 return response()->json(['success'=> false, 'message'=> 'Invoice cannot be deleted check if id exists']);

    }


}
