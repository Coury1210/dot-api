<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, DB, Hash, Mail;

use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        return Product::paginate(5);
    }
    public function store(Request $request)
    { 
        $credentials = $request->only('name','price','description','service');
//create rules for validation
      $rules = [
          'name' => 'required|max:255',
          'price'=> 'required',
          'description'=> 'required',
     
];
    //create validation varianles by requesting posted server parameter values       
      $name = $request->name;
      $price = $request->price;
      $description = $request->description;
      $service = $request->service;

      //create a validator for given rules
      $validator = Validator::make($credentials, $rules);
      //check for duplicate data
      $check = DB::table('products')->where('name',$name)->first();

//create if and not statements to kill processes and execute errors
      if($validator->fails()) {
          return response()->json(['success'=> false, 'error'=> $validator->messages()]);
      }

      elseif(is_null($name)){
         
              return response()->json([
                                          'success'=> false,
                                          'message'=> 'Enter product name, field is empty',
                                      ]);
          }
          elseif(is_null($price)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter product price field is empty',
                                    ]);
        }
        elseif(!is_null($check)){
           
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Product ' . $name.' already exists',
                                    ]);
        }
          DB::table('products')->insert(['name'=>$name,'price'=>$price,'description'=>$description,'service'=>$service,]);
       
        return response()->json(['success'=> true, 'message'=> 'Product' . $name . 'has been stored successfully']);

        }

    public function show(Product $product)
    {
        return $product;
    }
    public function update(Request $request)
    {//variables to resouce data from the server go here
        $buyer=$request->id;
        $update =$request->price;
        
    //we make sure that the passed variables exist in the table
        $viewRecords = DB::table('products')->where('id',$buyer)->first();

     //function to execute if the user id is not found in the database   
        if(is_null($buyer)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter a product id to update',
                                    ]);
        }
        elseif(is_null($update)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter product price',
                                    ]);
        }
        elseif(is_null($viewRecords)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'There is no product with id ' .$buyer. ' please check product id to update',
                                    ]);
        }
        DB::table('products')->where('price', $buyer)->update(['price' =>$update ]);
        return response()->json(['success'=> true, 'message'=> 'Product details have been updated successfully']);

    }

    public function destroy(Request $request)
    {
        $owner = $request->id;
        $record = DB::table('products')->where('id',$owner)->first();
//validate user input if fields are empty
        if(is_null($owner)){
            return response()->json(['success'=> false, 'message'=> 'Please provide the product id']);
        }
        elseif(is_null($record)){
            return response()->json(['success'=> false, 'message'=> 'Product doesnot exist or was already deleted']);
        }
 //if fields are not empty then delete the requested data
        $data = DB::table('products')->where('id',$owner)->delete();

       if(!is_null($data)){
   //return a success message after delete      
            return response()->json([
                                        'success'=> true,
                                        'message'=> 'The product has been deleted ']);
        }
       // if use does not satisfy the verification send an error message 
 return response()->json(['success'=> false, 'message'=> 'Product cannot be deleted check if id exists']);

    }
    }

