<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, DB, Hash, Mail;

use App\Customer;

class CustomerController extends Controller
{
    //create functions to execute http requests when routed
    public function index()
    {
        return Customer::paginate(5);
    }
    public function store(Request $request)
      { 
          $credentials = $request->only('firstname','lastname', 'email', 'phone','address');
//create rules for validation
        $rules = [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users'];
      //create validation varianles by requesting posted server parameter values       
        $fname = $request->firstname;
        $lname = $request->lastname;
        $email = $request->email;
//check table to see if the given email was already registered
        $check = DB::table('customers')->where('email',$email)->first();
//create a validator for given rules
        $validator = Validator::make($credentials, $rules);
//create if and not statements to kill processes and execute errors
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        elseif(!is_null($check)){
           
                return response()->json([
                                            'success'=> false,
                                            'message'=> 'Customer with email' . $email.' already exists',
                                        ]);
            }
      
        DB::table('customers')->insert(['firstName'=>$fname,'lastName'=>$lname,
       'email'=>$email,'phone'=>$request->phone, 'address'=>$request->address,]);
       
        return response()->json(['success'=> true, 'message'=> 'The customer ' . $fname .' has been registered successfully']);

    }
//display customers 
    public function show(Request $request)
    {
    $fname = $request->id;
    $viewRecords = DB::table('customers')->where('id',$fname)->first();
    if(is_null($fname)){
         
        return response()->json([
                                    'success'=> false,
                                    'message'=> 'Enter customer id to show their records',
                                ]);
    }
    elseif(is_null($viewRecords)){
     
        return response()->json([
                                    'success'=> false,
                                    'message'=> 'There is no customer ' .$fname. ' please try again',
                                ]);
    }

    $result= DB::table('customers')
                   ->where('id', $fname)
                      ->get();
         return $result->toArray();
    }
    //update customer details with validation
    public function update(Request $request)
    {
    //now create variables that will resource or pick data from the server as the user posts it
        $buyer=$request->id;
        $update =$request->name;
        $viewRecords = DB::table('customers')->where('id',$buyer)->first();
        if(is_null($buyer)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter a customer id number to update',
                                    ]);
        }
    //if user does not enter a name value return an error
        elseif(is_null($update)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'Enter customer name to update records',
                                    ]);
        }
    //if the given information does not match up with the database query return error message
        elseif(is_null($viewRecords)){
         
            return response()->json([
                                        'success'=> false,
                                        'message'=> 'There is no customer with id ' .$buyer. ' please check customer id',
                                    ]);
        }
        //then update the validated information too the records
        DB::table('customers')->where('id', $buyer)->update(['firstName' =>$update ]);
        return response()->json(['success'=> true, 'message'=> 'Customer details have been updated successfully']);

    }
    //api route to delete customer resource

    public function destroy(Request $request)
    {
        $owner = $request->id;
        $record = DB::table('customers')->where('id',$owner)->first();

        if(is_null($owner)){
            return response()->json(['success'=> false, 'message'=> 'Please provide the customer id']);
        }
        elseif(is_null($record)){
            return response()->json(['success'=> false, 'message'=> 'Customer doesnot exist or was already deleted']);
        }
        $data = DB::table('customers')->where('id',$owner)->delete();

       if(!is_null($data)){
         
            return response()->json([
                                        'success'=> true,
                                        'message'=> 'The customer has been deleted ']);
        }
 return response()->json(['success'=> false, 'message'=> 'Customer cannot be deleted check if id exists']);

    }
    }

